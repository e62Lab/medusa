#ifndef MEDUSA_BITS_INTERPOLANTS_SHEPPARD_FWD_HPP_
#define MEDUSA_BITS_INTERPOLANTS_SHEPPARD_FWD_HPP_

/**
 * @file
 * Declaration of class for Sheppard's interpolant.
 *
 * @example test/interpolants/SheppardInterpolant_test.cpp
 */

#include <medusa/bits/spatial_search/KDTree.hpp>
#include <medusa/Types_fwd.hpp>

namespace mm {

/**
 * Scattered interpolant using a slightly modified Sheppard's interpolation (inverse distance
 * weighting).
 *
 * The essential difference is the introduction of a "regularization" parameter (`reg`) that is used
 * when computing the inverse distance weights:
 * \f$ w_i = 1 / (d(x, x_i)^{power} + reg) \f$
 * By supplying a positive regularization parameter one can avoid singularities at the locations of
 * the data points as well as control the "smoothness" of the interpolation (e.g., make the weights
 * of the neighbors less varied). The "smoothness" of interpolation can also be controlled by the
 * power parameter (`power`). The interpolation is not continuous due to the cut off at the closest
 * neighbors.
 * 
 * For vector output types, each component is interpolated in the same manner.
 * 
 * @tparam vec_t input type for interpolation
 * @tparam value_t output type for interpolation
 *
 * @sa PUApproximant
 *
 * Usage example:
 * @snippet interpolants/SheppardInterpolant_test.cpp Sheppard interpolant usage example
 * @ingroup interpolants
 */
template <class vec_t, class value_t>
class SheppardInterpolant {
  private:
    KDTree<vec_t> tree;                         ///< Tree of all points.
    Range<value_t> values;                      ///< Function values at given points.
    typedef typename vec_t::scalar_t scalar_t;  ///< Scalar data type.

  public:
    /**
     * Construct a new empty Sheppard Interpolant object. Use @ref setValues and
     * @ref setPositions() to fill the object.
     */
    SheppardInterpolant() = default;

    /**
     * Construct a new Sheppard Interpolant. Use @ref setValues to add values.
     * @param pos Positions.
     */
    explicit SheppardInterpolant(const Range<vec_t>& pos) : tree(pos) {
        int N = pos.size();
        assert_msg(N > 0, "Number of positions in the tree must be greater than 0, got %d.", N);
    }

    /**
     * Construct a new Sheppard Interpolant object.
     * @param pos Initial positions.
     * @param values Function values to be interpolated. Must support iteration.
     */
    template <class values_container_t>
    SheppardInterpolant(const Range<vec_t>& pos, const values_container_t& values) : tree(pos) {
        int N = pos.size();
        int M = values.end() - values.begin();

        assert_msg(N > 0, "Number of positions in the tree must be greater than 0, got %d.", N);
        assert_msg(N == M,
                   "Number of positions must equal number of values. Got %d positions in the tree "
                   "and %d values.",
                   N, M);

        this->values = Range<value_t>(values.begin(), values.end());
    }

    /**
     * Construct a new Sheppard Interpolant object. 
     * The value type must match the vector type of the vector field.
     * @param pos Initial positions.
     * @param field Function values to be interpolated.
     */
    template <class scalar_t, int dim>
    SheppardInterpolant(const Range<vec_t>& pos, const VectorField<scalar_t, dim>& field)
            : tree(pos) {
        int N = pos.size();
        int M = field.rows();

        assert_msg(N > 0, "Number of positions in the tree must be greater than 0, got %d.", N);
        assert_msg(N == M,
                   "Number of positions must equal number of values. Got %d positions in the tree "
                   "and %d values.",
                   N, M);

        values.resize(M);
        for (int i = 0; i < M; ++i) {
            values[i] = field[i];
        }
    }

    /**
     * Set new values. Any container that supports forward iteration with `.begin()` and `.end()`
     * can be used. The contained element type must be compatible with `value_t`.
     */
    template <typename values_container_t>
    void setValues(const values_container_t& new_values) {
        values = Range<value_t>(new_values.begin(), new_values.end());
    }

    /**
     * Set new values from a vector field.
     * The value type must match the vector type of the vector field.
     */
    template <class scalar_t, int dim>
    void setValues(const VectorField<scalar_t, dim>& field) {
        values.resize(field.rows());
        for (int i = 0; i < field.rows(); ++i) {
            values[i] = field[i];
        }
    }

    /// Set new positions.
    void setPositions(const Range<vec_t>& pos) { tree.reset(pos); }

    /**
     * Evaluate the interpolant at the given point.
     *
     * The complexity of the evaluation function is
     * \f$ \mathcal{O}(m\log{n}) \f$
     * for \f$ m \f$ closest nodes and \f$ n \f$ positions in the k-d tree.
     * 
     * The approximation order equals \f$ O(h) \f$.
     *
     * @param point Location where evaluation is needed.
     * @param num_closest Number of closest neighbors.
     * @param power The power of the inverse distance used for the interpolation weights.
     * @param reg Regularization parameter.
     * @param conf_dist The confusion distance below which the interpolator should use the value of
     * the closest data point instead of attempting to interpolate.
     * @return value_t Interpolation value.
     */
    value_t operator()(const vec_t& point, int num_closest, int power = 2, double reg = 0.0,
                       double conf_dist = 1e-12) const;
};

}  // namespace mm

#endif  // MEDUSA_BITS_INTERPOLANTS_SHEPPARD_FWD_HPP_
