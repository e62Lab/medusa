#ifndef MEDUSA_BITS_DOMAINS_POLYTOPESHAPE_FWD_HPP_
#define MEDUSA_BITS_DOMAINS_POLYTOPESHAPE_FWD_HPP_

#include <medusa/Config.hpp>
#include "DomainShape_fwd.hpp"
#include "PolygonShape_fwd.hpp"

/**
 * @file
 * Declaration of PolytopeShape.
 *
 * @example test/domains/PolytopeShape_test.cpp
 */

namespace mm {

/// Auxiliary helper struct to support template parameter specialisation for PolytopeShape alias.
/// This is required, as specialising on template parameters on an alias is not supported.
template <typename Scalar, int U>
struct PolytopeShapeAuxStruct{
    static_assert(U == 2 || U == 3, "Only available in 2 and 3 dimensions");
};

/// Specialisation for dimension 2.
template <typename Scalar>
struct PolytopeShapeAuxStruct<Scalar, 2>{
    /// Dimension 2 PolytopeShape specialises to PolygonShape
    using Type = PolygonShape<Vec<Scalar, 2>>;
};

/// Specialisation for dimension 3.
template <typename Scalar>
struct PolytopeShapeAuxStruct<Scalar, 3>;

/**
 * Shape representing a simple nonempty polytope (i.e., non-self intersecting polygon in
 * 2D and polyhedron in 3D.). Can be used as a generic dimension independent
 * substitute for PolygonShape or PolyhedronShape.
 *
 * @tparam vec_t This shape alias is used only for 2D and 3D domains. General polytopes are not
 * supported.
 *
 * @warning 3D version depends on CGAL and is not included by default. You must
 * explicitly include PolyhedronShape.hpp.
 *
 * Usage example:
 * @snippet domains/PolytopeShape_test.cpp PolytopeShape usage example
 *
 * @ingroup domains
 * @sa PolygonShape, PolyhedronShape
 */
template <typename vec_t>
using PolytopeShape = typename PolytopeShapeAuxStruct<typename vec_t::scalar_t, vec_t::dim>::Type;

}  // namespace mm

#endif  // MEDUSA_BITS_DOMAINS_POLYTOPESHAPE_FWD_HPP_
