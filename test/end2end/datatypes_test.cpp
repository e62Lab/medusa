#include <medusa/bits/approximations/RBFFD.hpp>
#include <medusa/bits/approximations/Gaussian.hpp>
#include <medusa/bits/approximations/Operators.hpp>
#include <medusa/bits/approximations/Monomials.hpp>
#include <medusa/bits/approximations/Polyharmonic.hpp>
#include <medusa/bits/approximations/RBFBasis.hpp>
#include <medusa/bits/approximations/WLS.hpp>
#include <medusa/bits/approximations/JacobiSVDWrapper.hpp>
#include <medusa/bits/approximations/ScaleFunction.hpp>
#include <medusa/bits/approximations/WeightFunction.hpp>
#include <medusa/bits/domains/GeneralFill_fwd.hpp>
#include <medusa/bits/domains/BallShape.hpp>
#include <medusa/bits/domains/BoxShape.hpp>
#include <medusa/bits/domains/GridFill.hpp>
#include <medusa/bits/domains/DomainDiscretization_fwd.hpp>
#include <medusa/bits/domains/FindClosest.hpp>
#include <medusa/bits/spatial_search/KDGrid.hpp>
#include <medusa/bits/types/Vec.hpp>

#include <Eigen/Cholesky>
#include <Eigen/LU>

#include "gtest/gtest.h"

#define dim 2

namespace mm {

TEST(End2end, Float) {
    typedef float scal_t;
    typedef Vec<scal_t, dim> vec_t;

    Gaussian<scal_t> g(2);
    RBFFD<Gaussian<scal_t>, vec_t> appr(g);

    Polyharmonic<scal_t, 3> phs;
    Monomials<vec_t> mon(2);
    RBFFD<decltype(phs), vec_t, NoScale,
        Eigen::PartialPivLU<Eigen::MatrixX<scal_t>>> appr_r(phs, mon);


    auto sh = BoxShape<vec_t>(-1, 1) - BallShape<vec_t>(0, 0.5);
    auto bsh = BoxShape<vec_t>(-1, 1);

    DomainDiscretization<vec_t> domain(sh);
    domain.fill(GridFill<vec_t>(-1, 1), 0.5);

    domain = DomainDiscretization<vec_t>(bsh);
    domain.fill(GridFill<vec_t>(-1, 1), 0.5);

    domain = sh.discretizeWithStep(0.1);
    domain = sh.discretizeBoundaryWithStep(0.1);

    vec_t point = 0.0;
    scal_t h = 0.123;
    Range<vec_t> support = {point, {0, h}, {h, 0}, {-h, 0}, {0, -h},
                                   {-h, h}, {h, h}, {-h, -h}, {h, -h}};

    WLS<Monomials<vec_t>, NoWeight<vec_t>, ScaleToClosest,
            Eigen::PartialPivLU<Eigen::Matrix<scal_t,
            Eigen::Dynamic, Eigen::Dynamic>>> wls(Monomials<vec_t>::tensorBasis(2), {});

    wls.compute({0.0, 0.0}, support);
    Eigen::Vector<scal_t, Eigen::Dynamic> shape =
        wls.getShape(Der2<2>(0)) + wls.getShape(Der2<2>(1, 1));
    Eigen::Vector<scal_t, Eigen::Dynamic> expected(9);
    expected << -4/h/h, 1/h/h, 1/h/h, 1/h/h, 1/h/h, 0, 0, 0, 0;
    EXPECT_EQ(expected, shape);

    shape = wls.getShape(Der1<2>(0));
    expected << 0, 0, 1/(2*h), -1/(2*h), 0, 0, 0, 0, 0;
    EXPECT_EQ(expected, shape);

    shape = wls.getShape(Der1<2>(1));
    expected << 0, 1/(2*h), 0, 0, -1/(2*h), 0, 0, 0, 0;
    EXPECT_EQ(expected, shape);

    shape = wls.getShape(Der2<2>(0, 1));
    expected << 0, 0, 0, 0, 0, -1./4/h/h, 1./4/h/h, 1./4/h/h, -1./4/h/h;
    EXPECT_EQ(expected, shape);
}


TEST(End2end, Double) {
    typedef double scal_t;
    typedef Vec<scal_t, dim> vec_t;

    Gaussian<scal_t> g(2);
    RBFFD<Gaussian<scal_t>, vec_t> appr(g);

    Polyharmonic<scal_t, 3> phs;
    Monomials<vec_t> mon(2);
    RBFFD<decltype(phs), vec_t, NoScale,
        Eigen::PartialPivLU<Eigen::MatrixX<scal_t>>> appr_r(phs, mon);


    auto sh = BoxShape<vec_t>(-1, 1) - BallShape<vec_t>(0, 0.5);
    auto bsh = BoxShape<vec_t>(-1, 1);

    DomainDiscretization<vec_t> domain(sh);
    domain.fill(GridFill<vec_t>(-1, 1), 0.5);

    domain = DomainDiscretization<vec_t>(bsh);
    domain.fill(GridFill<vec_t>(-1, 1), 0.5);

    domain = sh.discretizeWithStep(0.1);
    domain = sh.discretizeBoundaryWithStep(0.1);

    vec_t point = 0.0;
    scal_t h = 0.123;
    Range<vec_t> support = {point, {0, h}, {h, 0}, {-h, 0}, {0, -h},
                                   {-h, h}, {h, h}, {-h, -h}, {h, -h}};

    WLS<Monomials<vec_t>, NoWeight<vec_t>, ScaleToClosest,
            Eigen::PartialPivLU<Eigen::Matrix<scal_t, Eigen::Dynamic, Eigen::Dynamic>>>
            wls(Monomials<vec_t>::tensorBasis(2), {});

    wls.compute({0.0, 0.0}, support);
    Eigen::Vector<scal_t, Eigen::Dynamic> shape = wls.getShape(Der2<2>(0))
        + wls.getShape(Der2<2>(1, 1));
    Eigen::Vector<scal_t, Eigen::Dynamic> expected(9);
    expected << -4/h/h, 1/h/h, 1/h/h, 1/h/h, 1/h/h, 0, 0, 0, 0;
    EXPECT_EQ(expected, shape);

    shape = wls.getShape(Der1<2>(0));
    expected << 0, 0, 1/(2*h), -1/(2*h), 0, 0, 0, 0, 0;
    EXPECT_EQ(expected, shape);

    shape = wls.getShape(Der1<2>(1));
    expected << 0, 1/(2*h), 0, 0, -1/(2*h), 0, 0, 0, 0;
    EXPECT_EQ(expected, shape);

    shape = wls.getShape(Der2<2>(0, 1));
    expected << 0, 0, 0, 0, 0, -1./4/h/h, 1./4/h/h, 1./4/h/h, -1./4/h/h;
    EXPECT_EQ(expected, shape);
}

TEST(End2end, LongDouble) {
    typedef long double scal_t;
    typedef Vec<scal_t, dim> vec_t;

    Gaussian<scal_t> g(2);
    RBFFD<Gaussian<scal_t>, vec_t> appr(g);

    Polyharmonic<scal_t, 3> phs;
    Monomials<vec_t> mon(2);
    RBFFD<decltype(phs), vec_t, NoScale,
        Eigen::PartialPivLU<Eigen::MatrixX<scal_t>>> appr_r(phs, mon);


    auto sh = BoxShape<vec_t>(-1, 1) - BallShape<vec_t>(0, 0.5);
    auto bsh = BoxShape<vec_t>(-1, 1);

    DomainDiscretization<vec_t> domain(sh);
    domain.fill(GridFill<vec_t>(-1, 1), 0.5);

    domain = DomainDiscretization<vec_t>(bsh);
    domain.fill(GridFill<vec_t>(-1, 1), 0.5);

    domain = sh.discretizeWithStep(0.1);
    domain = sh.discretizeBoundaryWithStep(0.1);

    vec_t point = 0.0;
    scal_t h = 0.123;
    Range<vec_t> support = {point, {0, h}, {h, 0}, {-h, 0}, {0, -h},
                                   {-h, h}, {h, h}, {-h, -h}, {h, -h}};

    WLS<Monomials<vec_t>, NoWeight<vec_t>, ScaleToClosest,
            Eigen::PartialPivLU<Eigen::Matrix<scal_t, Eigen::Dynamic, Eigen::Dynamic>>>
            wls(Monomials<vec_t>::tensorBasis(2), {});

    wls.compute({0.0, 0.0}, support);
    Eigen::Vector<scal_t, Eigen::Dynamic> shape = wls.getShape(Der2<2>(0))
        + wls.getShape(Der2<2>(1, 1));
    Eigen::Vector<scal_t, Eigen::Dynamic> expected(9);
    expected << -4/h/h, 1/h/h, 1/h/h, 1/h/h, 1/h/h, 0, 0, 0, 0;
    EXPECT_EQ(expected, shape);

    shape = wls.getShape(Der1<2>(0));
    expected << 0, 0, 1/(2*h), -1/(2*h), 0, 0, 0, 0, 0;
    EXPECT_EQ(expected, shape);

    shape = wls.getShape(Der1<2>(1));
    expected << 0, 1/(2*h), 0, 0, -1/(2*h), 0, 0, 0, 0;
    EXPECT_EQ(expected, shape);

    shape = wls.getShape(Der2<2>(0, 1));
    expected << 0, 0, 0, 0, 0, -1./4/h/h, 1./4/h/h, 1./4/h/h, -1./4/h/h;
    EXPECT_EQ(expected, shape);
}

}  // namespace mm
