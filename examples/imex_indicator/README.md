# IMplicit-EXplicit (IMEX) error indicator

Example of IMEX indicator on a Poisson Peak problem. For details, please refer
to [this paper](https://ieeexplore.ieee.org/document/9854342).