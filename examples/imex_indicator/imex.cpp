#include <medusa/Medusa_fwd.hpp>
#include <medusa/bits/domains/GeneralFill.hpp>
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include <math.h>

/// IMplicit-EXplicit error indicator example demonstrated on a Poisson Peak problem.
/// https://ieeexplore.ieee.org/document/9854342

using namespace mm;  // NOLINT

/**
 * @brief Returns value of Binomial Coefficient C(n, k).
 * https://www.geeksforgeeks.org/binomial-coefficient-dp-9/
 *
 * @param n
 * @param k
 * @return int
 */
int binomialCoeff(int n, int k) {
    // Base Cases.
    if (k > n) return 0;
    if (k == 0 || k == n) return 1;

    // Recursion.
    return binomialCoeff(n - 1, k - 1) + binomialCoeff(n - 1, k);
}

/**
 * Analytic solution of Peak problem.
 * @tparam vec_t Vector.
 * @param p Position.
 * @param alpha Source parameter.
 * @return Scalar value as solution.
 */
template <typename vec_t>
typename vec_t::scalar_t u_ana(const vec_t& p, double alpha) {
    vec_t source;
    for (int i = 0; i < vec_t::dim; ++i) {
        source[i] = 1.0 / (static_cast<double>(i) + 2.0);
    }
    double squared_norm = (p - source).squaredNorm();
    double exponent = -alpha * squared_norm;

    return exp(exponent);
}

/**
 * Analytic laplacian of Peak problem.
 * @tparam vec_t Vector.
 * @param p Position.
 * @param alpha Source parameter.
 * @return Scalar value (laplacian).
 */
template <typename vec_t>
typename vec_t::scalar_t u_lap(const vec_t& p, double alpha) {
    vec_t source;
    for (int i = 0; i < vec_t::dim; ++i) {
        source[i] = 1.0 / (static_cast<double>(i) + 2.0);
    }
    double squared_norm = (p - source).squaredNorm();
    double exponent = -alpha * squared_norm;

    return exp(exponent) * (mm::ipow<2>(alpha * 2.0) * squared_norm - 2.0 * alpha * vec_t::dim);
}

/**
 * Analytic gradient to Peak problem.
 * @tparam vec_t Vector.
 * @param p Position.
 * @param alpha Source parameter.
 * @return Vector, i.e. analytic gradient.
 */
template <typename vec_t>
vec_t u_grad(const vec_t& p, double alpha) {
    vec_t source;
    for (int i = 0; i < vec_t::dim; ++i) {
        source[i] = 1.0 / (static_cast<double>(i) + 2.0);
    }
    double squared_norm = (p - source).squaredNorm();
    double exponent = -alpha * squared_norm;

    double constant = -2 * alpha * exp(exponent);

    return constant * (p - source);
}

int main() {
    // Parameters.
    const double h = 0.01;                           // Internodal distance.
    const int approximation_order = 2;               // Approximation order.
    const int imex_order = approximation_order + 2;  // Order of explicit re-evaluation.
    const double source_strength = 1000;             // Exponential peak width.

    // ************
    // Make domain.
    // ************
    BallShape<Vec2d> ball(0.5, 0.5);
    auto dx = [&](const Vec2d& p) { return h; };
    auto domain = ball.discretizeBoundaryWithDensity(dx);
    // Fill interior.
    GeneralFill<Vec2d> fill;
    int fill_seed = 15;  // Fill seed.
    fill.seed(fill_seed);
    fill(domain, dx, 1);
    int N = domain.size();

    // Define node sets. (Dirichlet if x < 0.5 and Neumann otherwise.)
    auto interior = domain.interior();
    auto boundary = domain.boundary();
    Range<int> neumann, dirichlet;
    for (int i : boundary) {
        auto pos = domain.pos(i);

        if (pos[0] < 0.5) {
            dirichlet.push_back(i);
        } else {
            neumann.push_back(i);
        }
    }
    // Print stats.
    std::cout << "Interior size: " << interior.size() << std::endl;
    std::cout << "Dirichlet size: " << dirichlet.size() << std::endl;
    std::cout << "Neumann size: " << neumann.size() << std::endl;

    // **************
    // Approximation.
    // **************
    // Find stencils.
    int support_size = 2 * binomialCoeff(approximation_order + Vec2d::dim, Vec2d ::dim);
    domain.findSupport(FindClosest(support_size));

    // Approximation engine.
    RBFFD<Polyharmonic<double>, Vec2d, ScaleToClosest> approx(3, approximation_order);

    // Compute shapes.
    auto storage = domain.computeShapes<sh::lap | sh::d1>(approx);

    // Matrix and rhs.
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    Eigen::VectorXd rhs(N);
    rhs.setZero();
    M.reserve(storage.supportSizes());

    // Construct implicit operators over our storage.
    auto op = storage.implicitOperators(M, rhs);

    // ********
    // Problem.
    // ********
    // Interior.
    for (int c : interior) {
        op.lap(c) = u_lap(domain.pos(c), source_strength);
    }

    // Dirichlet BC.
    for (int c : dirichlet) {
        op.value(c) = u_ana(domain.pos(c), source_strength);
    }

    // Neumann BC.
    for (int c : neumann) {
        Vec2d pos = domain.pos(c);
        Vec2d normal = domain.normal(c);
        Vec2d grad = u_grad(pos, source_strength);

        op.neumann(c, normal) = normal.dot(grad);
    }

    // **************
    // Solve problem.
    // **************
    Eigen::SparseLU<decltype(M)> solver;
    solver.compute(M);
    Eigen::VectorXd solution = solver.solve(rhs);
    if (solver.info() == Eigen::Success) {
        print_green("Solver converged!\n");
    } else {
        print_red("Solver did not converge!\n");
    }

    // ***************************
    // Evaluate implicit solution.
    // ***************************
    // Analytic solution.
    Eigen::VectorXd solution_analytic(N);
    for (int i = 0; i < N; ++i) {
        auto pos = domain.pos(i);

        solution_analytic[i] = u_ana(pos, source_strength);
    }

    // Error.
    auto solution_error = solution_analytic - solution;
    // Compute norms.
    double err1 = solution_error.lpNorm<1>() / solution_analytic.lpNorm<1>();
    double err2 = solution_error.lpNorm<2>() / solution_analytic.lpNorm<2>();
    double errinf =
        solution_error.lpNorm<Eigen::Infinity>() / solution_analytic.lpNorm<Eigen::Infinity>();
    std::cout << "Error l-1: " << err1 << std::endl;
    std::cout << "Error l-2: " << err2 << std::endl;
    std::cout << "Error l-inf: " << errinf << std::endl;

    // ****************************************************************
    // Explixcit re-evaluation of implicitly obtained solution. (IMEX).
    // ****************************************************************
    Eigen::VectorXd reevaluated_rhs(N);
    reevaluated_rhs.setZero();
    {
        // Find stencils.
        int support_size = 2 * binomialCoeff(imex_order + Vec2d::dim, Vec2d ::dim);
        domain.findSupport(FindClosest(support_size));

        // Approximation engine.
        RBFFD<Polyharmonic<double>, Vec2d, ScaleToClosest> approx_imex(3, imex_order);

        // Compute shapes.
        auto storage = domain.computeShapes<sh::lap | sh::d1>(approx_imex);

        // Operators.
        auto op = storage.explicitOperators();

        // Interior.
        for (int i : interior) {
            reevaluated_rhs[i] = op.lap(solution, i);
        }
        // Dirichlet.
        for (int i : dirichlet) {
            reevaluated_rhs[i] = solution[i];
        }
        // Neumann.
        for (int i : neumann) {
            Vec2d normal = domain.normal(i);
            Vec2d grad = op.grad(solution, i);

            reevaluated_rhs[i] = op.neumann(solution, i, normal, normal.dot(grad));
        }
    }

    // Indicator field.
    Eigen::VectorXd indicator = reevaluated_rhs - rhs;
    std::cout << "MAX indicator: " << indicator.cwiseAbs().maxCoeff() << std::endl;
    std::cout << "MEAN indicator: " << indicator.cwiseAbs().mean() << std::endl;

    // *************
    // Post-process.
    // *************
    // Store to HDF file.
    HDF out_file;
    out_file.open("results.h5", "/", HDF::DESTROY);
    out_file.writeDomain("domain", domain);
    out_file.writeEigen("solution", solution);
    out_file.writeEigen("error", solution_error);
    out_file.writeEigen("indicator", indicator);
    out_file.close();

    return 0;
}
